// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

// Get the header
var header = document.getElementById("header");
var eventImg = document.getElementById("eventImg");

// Get the offset position of the navbar
var borderBottom = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > borderBottom) {
    header.classList.add("header-scrolled");
    eventImg.classList.add("sticky");
  } else {
    header.classList.remove("header-scrolled");
    eventImg.classList.remove("sticky");
  }
} 